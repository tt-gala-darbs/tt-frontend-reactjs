module.exports = {  
    purge:['./src/**/*.{js,jsx,ts,tsx}','./src/index.html'],  
    darkMode: false, // or 'media' or 'class'  
    theme: {
        colors:{
          transparent: 'transparent',
          purple: {
            dark:"#7757B7",
            light:"#6F46B7"
          },
          blue:{
            dark:"#03547E"
          },
          pink:{
            light:"#D83F87"
          },
          white:{
            light:"#FFFFFF"
          },
          gray:{
            light:"#707070"
          }
        },
        boxShadow: {
          sm: '0 1px 2px 0 rgba(0, 0, 0, 0.05)',
          DEFAULT: '0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)',
          md: '0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)',
          lg: '0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)',
          xl: '0 20px 25px -5px rgba(0, 0, 0, 0.1), 0 10px 10px -5px rgba(0, 0, 0, 0.04)',
          '2xl': '0 25px 50px -12px rgba(0, 0, 0, 0.25)',
  
         '3xl': '7px 6px 7px 0 rgba(0, 0, 0, 0.3)',
          inner: 'inset 0 2px 4px 0 rgba(0, 0, 0, 0.06)',
          none: 'none',
        }
        
        },   
        extend: {
        },  
      variants: 
    {  extend: {
          grayscale: ['disabled'],

    },  
},
  plugins: [],};