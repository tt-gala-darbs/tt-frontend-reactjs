import React, {useState} from 'react';
import {IntlProvider} from 'react-intl';
import English from "../translation/en.json";
import Latvian from "../translation/lv.json";

const local = navigator.language;
let lang;
if (local === 'en') {
   lang = English;
}else {
    (local === 'lv') 
       lang = Latvian;
   
}
const Wrapper = (props) => {
   const [locale, setLocale] = useState(local);
   const [messages, setMessages] = useState(lang);
   function selectLanguage(e) {
       const newLocale = e.target.value;
       setLocale(newLocale);
       if (newLocale === 'en') {
           setMessages(English);
       } else {
           if (newLocale === 'lv'){
               setMessages(Latvian);
           } 
       }
   }
   return (
       <Context.Provider value = {{locale, selectLanguage}}>
           <IntlProvider messages={messages} locale={locale}>
               {props.children}
           </IntlProvider>
       </Context.Provider>
   );
}
export const Context = React.createContext();
export default Wrapper;

