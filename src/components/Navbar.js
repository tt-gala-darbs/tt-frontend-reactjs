import React from "react";
import { Link, Redirect } from "react-router-dom";
import { useHistory } from "react-router-dom";
import react, { useState, useEffect, useContext } from "react";
import { FormattedMessage } from "react-intl";
import {Context} from "./Wrapper";

const Navbar = ({ isOpen, toggle }, props) => {
  const [isLoading2, setisLoading2] = useState()
  const [isLoggedIn, setisLoggedIn] = useState()

  const username = localStorage.getItem("username");
  const token = localStorage.getItem("csrf-token");
  
 
  const context = useContext(Context)

  const url2 = localStorage.getItem("url2");
  const url = localStorage.getItem("url");
  let history = useHistory();
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState();
  useEffect(() => {
    fetch(`https://tt.ruza.lv/api/get-all-communities`, {})
  .then((data) => data.json())
  .then((response) => {
    setData(response);
    setIsLoading(false);
    if (username != null) {
      setisLoading2(false)
    } else {
      setisLoading2(true)
    }
  })
  
},[])

  

  
  const handleSignOut = (e) => {
    e.preventDefault();

    localStorage.clear();
    history.push("/login")
  };

  const [navbarOpen, setNavbarOpen] = React.useState(false);

  return (
    <>
      <nav className={!isLoading ? "relative flex flex-wrap items-center justify-between py-3 bg-gradient-to-br from-purple-dark to-blue-dark mb-3" : "hidden"}>
        <div className="container text-white-light px-4 mx-auto flex flex-wrap items-center justify-between">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-8 w-8 mr-4"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M3.055 11H5a2 2 0 012 2v1a2 2 0 002 2 2 2 0 012 2v2.945M8 3.935V5.5A2.5 2.5 0 0010.5 8h.5a2 2 0 012 2 2 2 0 104 0 2 2 0 012-2h1.064M15 20.488V18a2 2 0 012-2h3.064M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
            />
          </svg>
          <div className="w-full relative flex justify-between lg:w-auto lg:static lg:block lg:justify-start">
            <Link to="/home" className="text-sm font-bold leading-relaxed inline-block mr-4 py-2 whitespace-nowrap uppercase text-white-light">
              KPTR
            </Link>
          </div>
          <div className="relative inline-block text-left">
            <div>
              <button
                onClick={toggle}
                type="button"
                className="flex justify-center w-full mb-1 rounded-md px-4 py-2 bg-transparent text-md font-bold text-white-light hover:bg-pink-light focus:outline-none  "
                id="menu-button"
              >
               <FormattedMessage id="navbar.communities">
                Communities
                </FormattedMessage>
                <svg
                  className="-mr-1 ml-2 h-5 w-5"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                  aria-hidden="true"
                >
                  <path
                    fillRule="evenodd"
                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  />
                </svg>
              </button>
            </div>

            <div
              className={
                isOpen
                  ? "origin-top-right absolute right-100 top-12 mt-2 w-40 shadow-lg bg-gradient-to-br from-purple-dark to-blue-dark  "
                  : "hidden"
              }
              aria-orientation="vertical"
            >
              <div className="py-1">
                {!isLoading &&
                data.map((community_name) => {
                return (
                <Link to={`/community/${community_name}`}
                  className="text-white-light block px-4 py-2 hover:text-pink-light text-sm"
                  key={community_name}
                >
                  {community_name}
                </Link>
                )
})}
                
                
              </div>
            </div>
          </div>
          <div
            className={ 
              "lg:flex flex-grow items-center inline-block" +
              (navbarOpen ? " flex" : " hidden")
            }
          >
            <ul className="flex lg:ml-auto inline-block">
              <li className="nav-item inline-block">
                <Link to="/settings" className="px-3 py-2 flex items-center text-md font-bold text-white-light hover:text-pink-light">
                  <span className="ml-2">{username}</span>
                </Link>
                
              </li>
            </ul>
            <form onSubmit={handleSignOut} className="inline-block float-right">
                  <button
                    type="submit"
                    className={!isLoading2 ? `text-white-light block w-full text-left text-md font-bold px-4 py-2 hover:bg-pink-light rounded-md` : "hidden"}
                  >
                    <FormattedMessage id="navbar.signout">
                    Sign out
                    </FormattedMessage>
                  </button>
                </form>
                <form onSubmit={handleSignOut} className="inline-block float-right">
                  <button
                    type="submit"
                    className={isLoading2 ? `text-white-light block w-full text-left text-md font-bold px-4 py-2 hover:bg-pink-light rounded-md` : "hidden"}
                  >
                    <FormattedMessage id="navbar.signin">
                    Sign out
                    </FormattedMessage>
                  </button>
                </form>
                <div className="bg-none">
                <select className="flex justify-center w-full rounded-md px-2 py-2 bg-transparent font-bold hover:bg-pink-light" value = {context.locale} onChange={context.selectLanguage}>
                  
                  <option className="hover:bg-pink-light hover:text-white-light" value='en'>English</option>
                  <option className="hover:bg-pink-light hover:text-white-light" value='lv'>Latviešu</option>
              </select>
              </div>
          </div>
        </div>
      </nav>
    </>
  );
};

export default Navbar;