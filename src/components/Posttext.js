import React from 'react'

const Posttext = () => {

    let title = localStorage.getItem('title')
    let content = localStorage.getItem('post_content')
    let name = localStorage.getItem('name')
    

    return (
        <div className="flex justify-center items-center mt-10">
        <div className=" h-full w-1/3 bg-white-light rounded-3xl"> 

            <h2 className="ml-4 mt-4 inline-block">community 1</h2>
            <h2 className="inline-block ml-2 text-gray-light ">posted by {name} h ago</h2>
            <h1 className="ml-4 mt-2 font-bold">{title}</h1>
            <div className="pb-4">
            <p className="mt-4 mx-10">{content}</p>
            </div>
            <div className="flex items-end mb-4">
            <button>
            <svg xmlns="http://www.w3.org/2000/svg" 
                className="h-6 w-6 inline-block ml-4" 
                fill="none" 
                viewBox="0 0 24 24" 
                stroke="currentColor">
            <path strokeLinecap="round" 
                strokeLinejoin="round" 
                strokeWidth={2} 
                d="M7 8h10M7 12h4m1 8l-4-4H5a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v8a2 2 0 01-2 2h-3l-4 4z" />
            </svg>
            </button>

            <button>
            <svg xmlns="http://www.w3.org/2000/svg" 
                className="h-6 w-6 inline-block ml-4" 
                fill="none" 
                viewBox="0 0 24 24" 
                stroke="currentColor">
            <path strokeLinecap="round" 
                strokeLinejoin="round" 
                strokeWidth={2} 
                d="M14 10h4.764a2 2 0 011.789 2.894l-3.5 7A2 2 0 0115.263 21h-4.017c-.163 0-.326-.02-.485-.06L7 20m7-10V5a2 2 0 00-2-2h-.095c-.5 0-.905.405-.905.905 0 .714-.211 1.412-.608 2.006L7 11v9m7-10h-2M7 20H5a2 2 0 01-2-2v-6a2 2 0 012-2h2.5" />
            </svg>
            </button>
            </div>
        </div>
        </div>
    )
}

export default Posttext
