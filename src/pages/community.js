import React from "react";
import Navbar from "../components/Navbar";
import react, { useState, useEffect, useContext } from "react";
import { Link, Redirect } from "react-router-dom";
import { useHistory } from "react-router-dom";
import {FormattedMessage} from 'react-intl';
import {Context} from "../components/Wrapper";

function Community({match}) {
  const username = localStorage.getItem("username");
  const [pageisLoading, setpageIsLoading] = useState(true);
  const context = useContext(Context);
  const token = localStorage.getItem("csrf-token");
  const url2 = localStorage.getItem("url2");
  const url = localStorage.getItem("url");
  const {
    params: { community_name },
  } = match;
  

  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState();
  const [isLoading8, setIsLoading8] = useState(true);
  const [data8, setData8] = useState();
  const [isLoading4, setIsLoading4] = useState(true);
  const [data4, setData4] = useState();
  let history = useHistory();

  
  const handleDeletePost = (e) => {
    let post_id = (e.target.dataset.post_id)
    fetch(`https://tt.ruza.lv/api/delete-post`, {
      method: "POST",
      cache: "no-cache",
      body: JSON.stringify({
        "csrf-token":token,
        "post-id": post_id
      }),
    })
      .then((data) => data.json())
      .then((response) => {
        setData8(response);
        setIsLoading8(false);
        fetch(`https://tt.ruza.lv/api/retrieve-community-posts`, {
        method: "POST",
        cache: "no-cache",
        body: JSON.stringify({
          "community-name": community_name,
          "csrf-token":token
          
        }),
      })
        .then((data) => data.json())
  
        .then((response) => {
          setData(response.posts);
          setIsLoading(false);
          
        })
      })

  }


  const [isLoading7, setIsLoading7] = useState(true);
  const [data7, setData7] = useState();
  useEffect(() => {

    fetch(`https://tt.ruza.lv/api/is-admin`, {
      method: "POST",
      cache: "no-cache",
      body: JSON.stringify({
        "csrf-token":token
      }),
    })
      .then((data) => data.json())
      .then((response) => {
        setData7(response);
        setIsLoading7(false);
      })
      
      
    }, []);


    useEffect(() => {
      fetch(`https://tt.ruza.lv/api/retrieve-community-posts`, {
        method: "POST",
        cache: "no-cache",
        body: JSON.stringify({
          "community-name": community_name,
          "csrf-token":token
          
        }),
      })
        .then((data) => data.json())
  
        .then((response) => {
          setData(response.posts);
          setIsLoading(false);
          
        })
        
    }, [community_name]);
  


 





  const [like_status, setlike_status] = useState();
  let [postLoading, setpostLoading] = useState(false);
  const [post_iD, setPost_id] = useState();
  const [remove_like, setRemove_like] = useState(false);
  
  
  
  

  const handlePostlike = e => {
    
    let post_id = (e.target.dataset.post_id)
    let is_liked = (e.target.dataset.is_liked)

    setPost_id(post_id)
    
    e.target.disabled = true;
    
    
    fetch(`https://tt.ruza.lv/api/like-post`, {
       method: "POST",
       cache: "no-cache",
       body: JSON.stringify({
         "post-id": post_id,
         "csrf-token": token,
         "remove-like": is_liked.toString(),
       }),
     })
       .then((data) => data.json())
       .then((response) => {
        setlike_status(response);
        setpostLoading(false);
        fetch(`https://tt.ruza.lv/api/retrieve-community-posts`, {
        method: "POST",
        cache: "no-cache",
        body: JSON.stringify({
          "community-name": community_name,
          "csrf-token":token
          
        }),
      })
        .then((data) => data.json())
  
        .then((response) => {
          setData(response.posts);
          setIsLoading(false);
          setTimeout(function() {
            e.target.disabled = false;
          }, 100);
        })
      });  
  }
  
  const [isLoading5, setIsLoading5] = useState(true);
  const [data5, setData5] = useState();
  const [isLoading6, setIsLoading6] = useState(true);
  const [data6, setData6] = useState();
  

  useEffect(() => {
  fetch(`https://tt.ruza.lv/api/is-subscribed`, {
  method: "POST",
  cache: "no-cache",
  body: JSON.stringify({
    "community-name": community_name,
    "csrf-token":token,
    
  }),
})
  .then((data) => data.json())

  .then((response) => {
    setData5(response);
    setIsLoading5(false);
  })
},[])

const subScribe = (e) => {
  e.target.disabled = true


fetch(`https://tt.ruza.lv/api/is-subscribed`, {
  method: "POST",
  cache: "no-cache",
  body: JSON.stringify({
    "community-name": community_name,
    "csrf-token":token,
    
  }),
})
  .then((data) => data.json())

  .then((response) => {
    setData5(response);
    setIsLoading5(false);
    let remove_sub = response["is-subscribed"]
    fetch(`https://tt.ruza.lv/api/subscribe-to-community`, {
  method: "POST",
  cache: "no-cache",
  body: JSON.stringify({
    "community-name": community_name,
    "csrf-token":token,
    "remove-subscription":remove_sub
    
  }),
})
  .then((data) => data.json())

  .then((response) => {
    setData4(response);
    setIsLoading4(false);
    fetch(`https://tt.ruza.lv/api/is-subscribed`, {
  method: "POST",
  cache: "no-cache",
  body: JSON.stringify({
    "community-name": community_name,
    "csrf-token":token,
    
  }),
})
  .then((data) => data.json())

  .then((response) => {
    setData5(response);
    setIsLoading5(false);
    setTimeout(function() {
      e.target.disabled = false;
    }, 100);
  })
    
  })

  })

   
      
  
}



      

     

  const [isLoading2, setIsLoading2] = useState(true);
  const [data2, setData2] = useState();

   
     
  useEffect(() => {

    fetch(`https://tt.ruza.lv/api/get-community-description`, {
      method: "POST",
      cache: "no-cache",
      body: JSON.stringify({
        "community": community_name,
      }),
    })
      .then((data) => data.json())

      .then((response) => {
        setData2(response);
        setIsLoading2(false);
      })
      
      
    }, [community_name]);

   

   
  
  localStorage.setItem("community_name", community_name);
  const createTimestamp = (seconds) => {
    let years, days, hours, minutes
    if (seconds >= 31_556_926) {
      years = parseInt(Math.round(seconds/31_556_926))
      return `${years} years ago`
    }
    if (seconds >= 84_400){
      days = parseInt(Math.round(seconds/84_400))
      return `${days} days ago`
    }
    if (seconds >=3_600){
      hours = parseInt(Math.round(seconds/3_600))
      return `${hours} h ago`
    }
    if (seconds >= 60){
      minutes = parseInt(Math.round(seconds/60))
      return `${minutes} min ago`
    } 
    return `${seconds} s ago`
  }


  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => {
    setIsOpen(!isOpen);
  };
  return (
    <div className={!isLoading && !isLoading2 ? "min-h-screen bg-gradient-to-br from-purple-dark to-blue-dark " : "hidden"}>
      <Navbar isOpen={isOpen} to toggle={toggle} />

      <div className=" flex justify-center items-center mx-auto">
        <ul>
        <Link to="/new_post">
          <button
            className={ ` flex justify-center items-center  w-80 bg-white-light ml-12 py-2 text-lg text-purple-light font-bold rounded-full hover:shadow-3xl hover:text-pink-light  focus:outline-none `}
          >
            <FormattedMessage id="home.create">
            Create new post
            </FormattedMessage>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-8 w-8 relative left-10"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M12 6v6m0 0v6m0-6h6m-6 0H6"
              />
            </svg>
          </button>
          
          
        </Link>
        {
          <>
          <button
            className={!isLoading5 && !data5["is-subscribed"] ? `filter disabled:grayscale flex justify-center items-center  w-80 bg-white-light ml-12 py-2 text-lg text-purple-light font-bold rounded-full hover:shadow-3xl hover:text-pink-light  focus:outline-none mt-4 ` : "hidden"}
            onClick = {subScribe}
          >
            <FormattedMessage id="communities.subscribe"
            values={
              {
                community_name: community_name
              }
            }>
           Subscribe to {community_name}
           </FormattedMessage>
          </button>
          <button
            className={!isLoading5 && data5["is-subscribed"] ? `filter disabled:grayscale flex justify-center items-center  w-80 bg-white-light ml-12 py-2 text-lg text-purple-light font-bold rounded-full hover:shadow-3xl hover:text-pink-light  focus:outline-none mt-4 ` : "hidden"}
            onClick = {subScribe}
          >
            <FormattedMessage id="communities.unsubscribe"
            values={
              {
                community_name: community_name
              }
            }>
           Subscribe to {community_name}
           </FormattedMessage>
          </button>
              </>
          }
          </ul>
        <div className="mr-12">
          <h2 className="flex justify-center ml-6  bg-gradient-to-br from-purple-dark to-blue-dark text-center rounded-t-3xl h-16 w-72 text-white-light text-lg font-bold text-align">
            {community_name}
          </h2>
          
          {!isLoading2 && (
          <p className="flex justify-center ml-6  bg-white-light rounded-b-3xl w-72 text-lg text-center text-align px-2">
            {data2.description}
          </p>
          )} 
        </div>
      </div>
      
      <>
        {!isLoading && !isLoading7 &&
          data.map((posts) => {
             
            let is_liked = posts["is-liked"]
            let liked
            if (is_liked == true){
              liked = true
            } else {
              liked = false
            }
            
            let post_id = posts.id;
            return (
              <div
                className=" justify-center items-center pb-4"
                key={posts.id}
              >
                <div className="flex justify-center items-center mt-10">
                  <div className=" h-full w-1/3 bg-white-light rounded-3xl">
                    <Link to={`/post/${post_id}`}>
                      <h2 className="ml-4 mt-4 inline-block">{community_name}</h2>
                      <h2 className="inline-block ml-2 text-gray-light ">
                        <FormattedMessage id="communities.posted"
                        values={
                          {
                           author_name: posts["author-name"],
                           timestamp:createTimestamp(posts.timestamp)
                          }
                        }>
                        posted by {posts["author-name"]} {createTimestamp(posts.timestamp)}
                        </FormattedMessage>
                      </h2>
                      
                     

                      <h1 className="ml-4 mt-2 font-bold">{posts.title}</h1>
                      <div className="pb-4">
                        <p className="mt-4 mx-10">{posts["post-content"]}</p>
                      </div>
                      </Link>
                      
                     
                        <button className={ (posts["author-name"] == username) || data7["is-admin"] ? "inline-block float-right mt-2 mr-4 text-sm  px-2 rounded-2xl bg-gradient-to-br from-purple-dark to-blue-dark text-white-light " : "hidden"}
                      data-post_id={post_id}
                      onClick={handleDeletePost}>
                        <FormattedMessage id="communities.delete">
                      Delete
                      </FormattedMessage>
                      </button>
                      
                     
                      <div className="flex items-end mb-4">
                      <Link to={`/post/${post_id}`}>
                        <button>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="h-6 w-6 inline-block ml-4"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth={2}
                              d="M7 8h10M7 12h4m1 8l-4-4H5a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v8a2 2 0 01-2 2h-3l-4 4z"
                            />
                          </svg>
                        </button>
                        </Link>
                        <div className="ml-4">
                          <button
                          onClick={handlePostlike}
                          data-post_id={post_id}
                          data-is_liked={is_liked}
                          className={liked ? ` filter disabled:grayscale focus:outline-none bg-pink-light text-white-light rounded-2xl w-10 ` : ` filter disabled:grayscale focus:outline-none bg-gradient-to-br from-purple-dark to-blue-dark text-white-light rounded-2xl w-10 ` }>
                        <FormattedMessage id="communities.like">
                        Like
                        </FormattedMessage>
                          </button>
                          </div>
                        
                      </div>
                    
                  </div>
                </div>
              </div>
            );
          })}
      </>
    </div>
  );
}

export default Community;
