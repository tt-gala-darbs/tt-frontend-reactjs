import React from "react";
import Navbar from "../components/Navbar";
import { useState, useEffect, useRef } from "react";
import {Link, Redirect} from 'react-router-dom';
import { FormattedMessage } from "react-intl";

function PostComment({ match }) {

  const loggedIn = localStorage.getItem("status");
  const token = localStorage.getItem("csrf-token");
  const url = localStorage.getItem("url");
  const url2 = localStorage.getItem("url2");
  if (loggedIn != "OK") {
    return <Redirect to="/login"></Redirect>;
  }
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => {
    setIsOpen(!isOpen);
  };

  const {
    params: { post_id },
  } = match;
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState();

  useEffect(() => {
    fetch(`https://tt.ruza.lv/api/retrieve-post-content`, {
        method: "POST",
        cache: "no-cache",
        body: JSON.stringify({
        "post-id": post_id,
        })
    })
      .then((res) => res.json())
      .then((response) => {
        setData(response);
        setIsLoading(false);
      })
      
      
      
  }, [post_id]);
  const [isLoading2, setIsLoading2] = useState(true);
  const [data2, setData2] = useState();
  
  
  useEffect(() => {
    fetch(`https://tt.ruza.lv/api/retrieve-post-comments`, {
        method: "POST",
        cache: "no-cache",
        body: JSON.stringify({
        "post-id": post_id,
        })
    })
      .then((res) => res.json())
      .then((response) => {
        setData2(response);
        setIsLoading2(false);
      })
      
      
  }, [post_id]);


 console.log(post_id)
  const [isLoading3, setIsLoading3] = useState(true);
  const [data3, setData3] = useState();
  const handlepostComment = (e) =>{
        e.preventDefault()
        let comment = e.target.elements.comment?.value;
        fetch(`https://tt.ruza.lv/api/api/post-comment`, {
        method: "POST",
        cache: "no-cache",
        body: JSON.stringify({
        "content": comment,
        "parent-post": post_id,
        "parent-comment": comment_id ,
        "csrf-token": token
        })
    })
      .then((res) => res.json())
      .then((response) => {
        setData3(response);
        setIsLoading3(false);
        fetch(`https://tt.ruza.lv/api/retrieve-post-comments`, {
          method: "POST",
          cache: "no-cache",
          body: JSON.stringify({
          "post-id": post_id,
          })
      })
        .then((res) => res.json())
        .then((response) => {
          setData2(response);
          setIsLoading2(false);
          setComment_id(null)
        })
      })
      
     
     
      
     
    
   
  }
  
  const [comment_id, setComment_id] = useState();
  const handleSubcomment = e => {
    setComment_id(e.target.dataset.comment_id)
  }


  const createTimestamp = (seconds) => {
    let years, days, hours, minutes
    if (seconds >= 31_556_926) {
      years = parseInt(Math.round(seconds/31_556_926))
      return `${years} years ago`
    }
    if (seconds >= 84_400){
      days = parseInt(Math.round(seconds/84_400))
      return `${days} days ago`
    }
    if (seconds >=3_600){
      hours = parseInt(Math.round(seconds/3_600))
      return `${hours} h ago`
    }
    if (seconds >= 60){
      minutes = parseInt(Math.round(seconds/60))
      return `${minutes} min ago`
    } 
    return `${seconds} s ago`
  }


  return (
    <>
      <div className={!isLoading && !isLoading2 ? "min-h-screen bg-gradient-to-br from-purple-dark to-blue-dark pb-6" : "hidden"}>
        <Navbar isOpen={isOpen} to toggle={toggle} />
        {!isLoading && 
          
            (
              <>
                <div
                  className=" justify-center mt-10 items-center pb-2"
                  key={data.post_id}
                >
                  <div className="flex justify-center items-center mt-10">
                    <div className=" h-full w-1/3 bg-white-light">
                      
                        <h2 className="ml-4 mt-4 inline-block"></h2>
                        <h2 className="inline-block ml-2 text-gray-light ">
                          
                        <FormattedMessage id="communities.posted"
                        values={
                          {
                           author_name: data.author,
                           timestamp: createTimestamp(data.timestamp)
                          }
                        }>
                        posted by {data.author} {createTimestamp(data.timestamp)}
                        </FormattedMessage>
                        
                        </h2>
                        <h1 className="ml-4 mt-2 font-bold">{data.title}</h1>
                        <div className="pb-4">
                          <p className="mt-4 mx-10">{data["post-content"]}</p>
                        </div>
                     
                    </div>
                  </div>
                </div>
              </>
            
            )}
          <div className=" justify-center mt-10 items-center pb-10">
          <div className="flex justify-center items-center mt-10">
            <div className=" h-full w-1/3 bg-white-light">
              <h2 className="inline-block ml-4 text-lg">
               <FormattedMessage id="post.write">
               Write your own comment
               </FormattedMessage>
              </h2>
              <h1 className="ml-4 mt-2 font-bold"></h1>
              <div className="pb-4">
                <form className="" onSubmit={handlepostComment} >
                    <textarea className="mt-4 mx-10 resize-none focus:outline-none" 
                        type="text"
                        rows="1"
                        cols="50"
                        placeholder="What are your thoughts?"
                        id="comment">
                    </textarea>
                    <button
                            className={`inline-block float-right w-40 bg-white-light py-2 mr-4 mb-4 text-md bg-gradient-to-br from-purple-dark to-blue-dark text-white-light font-bold rounded-full hover:shadow-3xl  focus:outline-none `}>
                           <FormattedMessage id="post.comment">
                           Comment
                           </FormattedMessage>
                    </button>
                </form>
              </div>
              
            </div>
          </div>
        </div>
        {!isLoading2 && 
          
          data2.map((comments) => {
            let comment_id = comments.ID

            return (
            <>
                <div className="  flex justify-center items-center"
                key={comments.ID}>
                  <div className=" h-full w-1/3 bg-white-light">
                    
                      <h2 className="inline-block ml-2 text-gray-light ">
                        posted by {comments['author-name']} {createTimestamp(comments.timestamp)}
                      </h2>
                      
                        <button className="inline-block float-right pr-10 text-gray-light focus:outline-none underline hover:text-purple-dark"
                        data-comment_id = {comment_id}
                        value="gunga"
                        onClick={handleSubcomment}
                        type="submit">
                            Reply
                        </button>
                      
                      <div className="pb-4">
                        <p className="mt-4 mx-10">{comments.content}</p>
                      </div>
                      <div className="flex items-end mb-4">
          
                      </div>
                   
                  </div>
                  
                </div>
               
                    
              
              {!isLoading2 && 
              comments.children.map((subcomment) => {
                let subcomment_id = subcomment.ID
              return (
                <>
              <div
                className="justify-center items-center"
                key={subcomment_id}
                
              >
                <div className="flex justify-center items-center">
                  <div className="pl-12 h-full w-1/3 bg-white-light mx-12">
                    <div className="">
                    
                      <h2 className=" inline-block ml-2 text-gray-light ">
                        posted by {subcomment['author-name']} {createTimestamp(subcomment.timestamp)}
                      </h2>
                      </div>
                        <button className="inline-block float-right pr-10 text-gray-light focus:outline-none underline hover:text-purple-dark"
                        data-comment_id = {subcomment_id}
                        onClick={handleSubcomment}>
                          
                            Reply
                          
                        </button>
                      
                      <div className="pb-4">
                        <p className="mt-4 mx-10">{subcomment.content}</p>
                      </div>
                      <div className="flex items-end mb-4">
          
                      </div>
                   
                  </div>
                </div>
              </div>

              {!isLoading2 && 
              subcomment.children.map((sub2comment) => {
                let sub2comment_id = sub2comment.ID
              return (
                <>
              <div
                className="justify-center items-center"
                key={sub2comment_id}
                
              >
                <div className="flex justify-center items-center">
                  <div className="pl-24 h-full w-1/3 bg-white-light mx-12">
                    <div className="">
                    
                      <h2 className=" inline-block ml-2 text-gray-light ">
                        posted by {sub2comment['author-name']} {createTimestamp(sub2comment.timestamp)}
                      </h2>
                      </div>
                        <button className="inline-block float-right pr-10 text-gray-light focus:outline-none underline hover:text-purple-dark"
                        data-comment_id = {sub2comment_id}
                        onClick={handleSubcomment}>
                          
                            Reply
                          
                        </button>
                      
                      <div className="pb-4">
                        <p className="mt-4 mx-10">{sub2comment.content}</p>
                      </div>
                      <div className="flex items-end mb-4">
          
                      </div>
                   
                  </div>
                </div>
              </div>
              {!isLoading2 && 
              sub2comment.children.map((sub3comment) => {
                let sub3comment_id = sub3comment.ID
              return (
                <>
              <div
                className="justify-center items-center"
                key={sub3comment_id}
                
              >
                <div className="flex justify-center items-center">
                  <div className="pl-36 h-full w-1/3 bg-white-light mx-12">
                    <div className="">
                    
                      <h2 className=" inline-block ml-2 text-gray-light ">
                        posted by {sub3comment['author-name']} {createTimestamp(sub3comment.timestamp)}
                      </h2>
                      </div>
                      
                      <div className="pb-4">
                        <p className="mt-4 mx-10">{sub3comment.content}</p>
                      </div>
                      <div className="flex items-end mb-4">
          
                      </div>
                   
                  </div>
                </div>
              </div>
              </>
              
          
              )
              
              })}
              </>
              
          
              )
              
              })}
              
              </>
              
          
              )
              
              })}
              
            </>
            )
})}
      </div>
    </>
  );
}

export default PostComment;