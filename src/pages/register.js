import React from "react";
import { Link } from "react-router-dom";
import {FormattedMessage, FormattedHTMLMessage} from 'react-intl';
import { useHistory } from "react-router-dom";
const Register = () => {
  let history = useHistory();

  const url2 = localStorage.getItem("url2");
  const url = localStorage.getItem("url");
  
const handleRegister = (e) => {

  let username = e.target.elements.new_username?.value;
  let password = e.target.elements.new_password?.value;
  let password_repeat = e.target.elements.password_repeat?.value;

    if (password == password_repeat) {

    fetch(`https://tt.ruza.lv/api/register`, {
      method: "POST",
      cache: "no-cache",
      body: JSON.stringify({
        username: username,
        password: password,
      }),
    })
      .then((data) => data.json())
      .then(function (data) {
      if (data.status == "OK") {
       history.push("/login")
      } 
      else if (data["status-code" == "password-too-short"]) {
        alert("Your new password is too short")
      } 
      else if (data["status-code" == "account-already-exists"]) {
        alert("Account already exists")
      }
      else if (data["status-code" == "invalid-username"]) {
        alert("Invalid username")
      }
    })
  } else {
    alert("Re-enter your new password")
  }
  };

  
  return (
    <div className="h-screen flex bg-white font-sans font-family:Roboto">
      <div className="w-full max-w-md m-auto bg-gradient-to-br from-purple-dark to-blue-dark shadow-3xl py-4 px-16">
        <h1 className="text-2xl text-white-light font-bold text-primary mt-4 text-center">
          <FormattedMessage id="register.register">
          Register
          </FormattedMessage>
        </h1>
        <form onSubmit={handleRegister}>
          <div>
            <input
              type="text"
              className={`w-3/4 p-2 text-md text-white-light bg-transparent border-b border-gray-light hover:shadow-3xl  outline-none text-sm transition duration-150 ease-in-out mb-4`}
              id="new_username"
              placeholder=" Type your new username"
            />
          </div>
          <div>
            <input
              type="password"
              className={`w-3/4 p-2 bg-transparent text-md text-white-light border-b border-gray-light hover:shadow-3xl outline-none text-sm transition duration-150 ease-in-out mb-4`}
              id="new_password"
              placeholder=" Type your new password"
            />
          </div>
          <div>
            <input
              type="password"
              className={`w-3/4 p-2 bg-transparent text-md text-white-light border-b border-gray-light hover:shadow-3xl outline-none text-sm transition duration-150 ease-in-out mb-4`}
              id="password_repeat"
              placeholder=" Re-enter your new password"
            />
          </div>

          <div className="flex justify-center items-center mt-6">
            <button
              className={` w-2/3 bg-white-light py-2 px-4 text-md text-purple-light font-bold rounded-full hover:shadow-3xl  focus:outline-none `}
            >
             <FormattedMessage id="register.register">
              Register
              </FormattedMessage>
            </button>
          </div>

          <p className="text-xs text-white-light text-center font bold mt-2">
          <FormattedMessage id="register.account">
            Already have an account?
            </FormattedMessage>
            <Link to="/login" className="text-pink-light underline">
            <FormattedMessage id="register.sign">
              Sign in here.
            </FormattedMessage>
            </Link>
          </p>
        </form>
      </div>
    </div>
  );
};

export default Register;
