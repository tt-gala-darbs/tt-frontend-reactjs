import React from 'react'
import Navbar from '../components/Navbar'
import react, {useState, useEffect} from 'react';
import {Link, Redirect} from 'react-router-dom';
import { useHistory } from "react-router-dom";
import {FormattedMessage} from 'react-intl'


function Post() {
  let history = useHistory();
  const community_name = localStorage.getItem("community_name");
  const loggedIn = localStorage.getItem('status');
  const url2 = localStorage.getItem("url2");
  const url = localStorage.getItem("url");
    if (loggedIn != 'OK') {
        return <Redirect to="/login"></Redirect>
    }
    const [isOpen, setIsOpen] = useState(false)

    const toggle = () => {
        setIsOpen(!isOpen)
    };



    const handlePostSubmit = (f) => {
      f.preventDefault();

      const csrf_token = localStorage.getItem('csrf-token');
            let title = f.target.elements.title?.value;
            let content = f.target.elements.content?.value;
    
    
      fetch(`https://tt.ruza.lv/api/create-community-post`, {
        method: 'POST',
        cache: 'no-cache',
        body: JSON.stringify({
            'post-content': content,
            'post-title': title,
            'post-community': community_name,
            'csrf-token': csrf_token
        })
    }).then (data => 
       data.json()
     )
      .then(function(data) {
        if(data.status == 'OK'){
            history.push(`/community/${community_name}`)
        }
      })

    
    }
    

    return (
      
        <div className=" h-screen bg-gradient-to-br from-purple-dark to-blue-dark">
        <Navbar isOpen={isOpen} to toggle={toggle}/>
        
           
           <div  className="flex justify-center container ">
             <div className="w-40">
           <h2 className="mt-1 bg-white-light rounded-3xl text-center text-xl text-purple-light font-bold">
                <FormattedMessage id="new_post.create">
                 Create your own post 
                 </FormattedMessage>
              </h2>
              
         
          <div className="float-left mt-4">
            <form className="" onSubmit={handlePostSubmit}>
              <textarea 
                className="resize-none text-lg "
                id="title" 
                type="text" 
                rows="1" 
                cols="50" 
                placeholder="Your title...">  
                </textarea>
              <textarea 
                className="resize-none text-lg" 
                id="content" 
                type="text" 
                rows="6" 
                cols="50" 
                placeholder="Your post...">
                </textarea>
                
              <button onSubmit={handlePostSubmit}
              className="float-left w-1/3 bg-white-light py-2 px-4 text-md text-purple-light font-bold rounded-full hover:shadow-3xl  focus:outline-none  mt-4">
                <FormattedMessage id="new_post.postbtn">
                Post
                </FormattedMessage>
              </button>
            
            </form>
           </div>
        </div>
          
        </div>
      
      
    </div>
    
    )
}

export default Post

