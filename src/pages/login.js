import React from "react";
import "../styles.css";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import {FormattedMessage, FormattedHTMLMessage} from 'react-intl';

const Login = (props) => {
  const url2 = "http://192.168.56.1/api"
  const url = "https://tt.ruza.lv/api"
  localStorage.setItem("url2", url2);
  localStorage.setItem("url", url);

  const handleFormSubmit = (e) => {
    
    e.preventDefault();

    let username = e.target.elements.username?.value;
    let password = e.target.elements.password?.value;

     const url = localStorage.getItem("url");
     const url2 = localStorage.getItem("url2");

    fetch(`https://tt.ruza.lv/api/login`, {
      method: "POST",
      cache: "no-cache",
      body: JSON.stringify({
        username: username,
        password: password,
      }),
    })
      .then((data) => data.json())

      .then(function (data) {
        let status = data.status;
        let token = data["csrf-token"];

        if (status == "OK") {
          localStorage.setItem("status", "OK");
          localStorage.setItem("username", username);
          localStorage.setItem("csrf-token", token);
        } else if (status == "FAILED") {
          localStorage.setItem("status", "FAILED");
          console.log(status);
        }
        if (status == "OK") {
          props.history.push("/home");
        }
        else {
          alert("Incorrect password/username")
        }
      });
  };
  return (
    <div className="h-screen flex bg-white font-sans font-family:Roboto">
      <div className="w-full max-w-md m-auto bg-gradient-to-br from-purple-dark to-blue-dark shadow-3xl py-4 px-16">
        
        <h1 className="text-2xl text-white-light font-bold text-primary mt-4 text-center">
        <FormattedMessage
        id="login.welcome"
        defaultMessage="Welcome1"
        >
          Welcome
          </FormattedMessage>
        </h1>
       
        <h2 className="text-md text-white-light text-center mb-10 font bold">
          <FormattedMessage
          id="login.started">
          Log in to get started!
          </FormattedMessage>
        </h2>
        <form onSubmit={handleFormSubmit}>
          <div>
            <input
              type="text"
              className={`w-2/3 p-2 text-lg text-white-light bg-transparent border-b border-gray-light hover:shadow-3xl  outline-none text-sm transition duration-150 ease-in-out mb-4`}
              id="username"
              placeholder=" Type your username"
            />
          </div>
          <div>
            <input
              type="password"
              className={`w-2/3 p-2 bg-transparent text-lg text-white-light border-b border-gray-light hover:shadow-3xl outline-none text-sm transition duration-150 ease-in-out mb-4`}
              id="password"
              placeholder=" Type your password"
            />
          </div>

          <div className="flex justify-center items-center mt-6">
            <button
              className={` w-2/3 bg-white-light py-2 px-4 text-md text-purple-light font-bold rounded-full hover:shadow-3xl  focus:outline-none `}
            >
              <FormattedMessage id="login.log_in"
              >
              Log In
              </FormattedMessage>
            </button>
          </div>

          <p className="text-xs text-white-light text-center font bold mt-2">
           <FormattedMessage id="login.first_time">
            First time here?
            </FormattedMessage>
            <Link to="/register" className="text-pink-light underline">
              <FormattedMessage id="login.create">
              Create a new account.
              </FormattedMessage>
            </Link>
          </p>
        </form>
      </div>
    </div>
  );
};

export default Login;
