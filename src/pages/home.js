import React from "react";
import Navbar from "../components/Navbar";
import react, { useState, useEffect } from "react";
import { Redirect, Link } from "react-router-dom";
import Login from "./login";
import { FormattedMessage } from "react-intl";

function Home() {
  
  const token = localStorage.getItem("csrf-token");
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => {
    setIsOpen(!isOpen);
  };
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState();
  const [isLoading2, setIsLoading2] = useState(true);
  const [data2, setData2] = useState();


 useEffect(() => {
    fetch(`https://tt.ruza.lv/api/retrieve-home-posts`, {
      method: "POST",
      cache: "no-cache",
      body: JSON.stringify({
        "csrf-token":token
        
      }),
    })
      .then((data) => data.json())

      .then((response) => {
        setData(response.posts);
        setIsLoading(false);
        
      })
    },[])
      
    const [post_iD, setPost_id] = useState();
    let [like_status, setlike_status] = useState();
    let [postLoading, setpostLoading] = useState(false);



    const handlePostlike = e => {

    if (like_status) {
      setlike_status(false)
    } else {
      setlike_status(true)
    }
      let post_id = (e.target.dataset.post_id)
      let is_liked = (e.target.dataset.is_liked)
     console.log(is_liked)
  
      setPost_id(post_id)
      
      e.target.disabled = true;
      
      
      fetch(`https://tt.ruza.lv/api/like-post`, {
         method: "POST",
         cache: "no-cache",
         body: JSON.stringify({
           "post-id": post_id,
           "csrf-token": token,
           "remove-like": is_liked.toString(),
         }),
       })
         .then((data) => data.json())
         .then((response) => {
          setlike_status(response);
          setpostLoading(false);
          
          
          fetch(`https://tt.ruza.lv/api/post-is-liked`, {
          method: "POST",
          cache: "no-cache",
          body: JSON.stringify({
            "post-id": post_id,
            "csrf-token": token
            
          }),
        })
          .then((data) => data.json())
    
          .then((response) => {
            setData2(response);
            setIsLoading2(false);
            setTimeout(function() {
              e.target.disabled = false;
            }, 100);
           
          })
        }); 
    }
  






    const createTimestamp = (seconds) => {
      let years, days, hours, minutes
      if (seconds >= 31_556_926) {
        years = parseInt(Math.round(seconds/31_556_926))
        return `${years} years ago`
      }
      if (seconds >= 84_400){
        days = parseInt(Math.round(seconds/84_400))
        return `${days} days ago`
      }
      if (seconds >=3_600){
        hours = parseInt(Math.round(seconds/3_600))
        return `${hours} h ago`
      }
      if (seconds >= 60){
        minutes = parseInt(Math.round(seconds/60))
        return `${minutes} min ago`
      } 
      return `${seconds} s ago`
    }


  return (
    <div className="min-h-screen bg-gradient-to-br from-purple-dark to-blue-dark">
      <Navbar isOpen={isOpen} to toggle={toggle} />
      <div className=" flex justify-center items-center mx-auto">
        
        
      </div>
      <>
        {!isLoading && 

          data.map((posts) => {
            let post_id = posts.id;
            let is_liked = posts["is-liked"]
            let liked
            
            if (is_liked == true){
              liked = true
            } else {
              liked = false
            }
            like_status = liked
            console.log(liked)
            return (
              <div
                className=" justify-center items-center pb-4"
                key={posts.id}
              >
                <div className="flex justify-center items-center mt-10">
                  <div className=" h-full w-1/3 bg-white-light rounded-3xl">
                    <Link to={`/post/${post_id}`}>
                      <h2 className="ml-4 mt-4 inline-block">{posts.community}</h2>
                      <h2 className="inline-block ml-2 text-gray-light ">
                        <FormattedMessage id="communities.posted"
                        values={
                          {
                           author_name: posts["author-name"],
                           timestamp:createTimestamp(posts.timestamp)
                          }
                        }>
                        posted by {posts["author-name"]} {createTimestamp(posts.timestamp)}
                        </FormattedMessage>
                      </h2>
                      <h1 className="ml-4 mt-2 font-bold">{posts.title}</h1>
                      <div className="pb-4">
                        <p className="mt-4 mx-10">{posts["post-content"]}</p>
                      </div>
                      </Link>
                      <div className="flex items-end mb-4">
                      <Link to={`/post/${post_id}`}>
                        <button>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="h-6 w-6 inline-block ml-4"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth={2}
                              d="M7 8h10M7 12h4m1 8l-4-4H5a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v8a2 2 0 01-2 2h-3l-4 4z"
                            />
                          </svg>
                        </button>
                        </Link>
                      </div>
                    
                  </div>
                </div>
              </div>
            );
          })}
        </>
    </div>
  );
}

export default Home;
