import React from 'react'
import { Link, Redirect } from "react-router-dom";
import react, { useState, useEffect } from "react";
import Navbar from "../components/Navbar";
import { FormattedMessage } from 'react-intl';
import { useHistory } from "react-router-dom";
const Settings = () => {
  let history = useHistory();
    const token = localStorage.getItem("csrf-token");

    const [isLoading, setIsLoading] = useState(true);
    const [data, setData] = useState();
  
    
  const handleChange = (e) => {
    let old_password = e.target.elements.old_password?.value;
    let new_password = e.target.elements.new_password?.value;
    let new_password_repeat = e.target.elements.new_password_repeat?.value;
    if (new_password == new_password_repeat) {
      fetch(`https://tt.ruza.lv/api/change-password`, {
      method: "POST",
      cache: "no-cache",
      body: JSON.stringify({
        "new-password": new_password,
        "current-password": old_password,
        "csrf-token": token
      }),
    })
    .then((data) => data.json())
    .then(function (data) {
      if (data.status == "OK") {
        localStorage.clear();
        history.push("/login")
      } else if (data['status-code'] == "incorrect-login") {
          alert("Incorrect current password")
      } else if (data['status-code'] == "password-too-short") {
          alert("New password is too short")
      }
    })
    
    } else {
      alert("Re-enter yoyur new password")
    }
    
  }
    
    

    const username = localStorage.getItem("username");
    const loggedIn = localStorage.getItem("status");
  if (loggedIn != "OK") {
    return <Redirect to="/login"></Redirect>;
  }
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => {
    setIsOpen(!isOpen);
  };
    return (
        <div className="min-h-screen bg-gradient-to-br from-purple-dark to-blue-dark ">
            <Navbar isOpen={isOpen} to toggle={toggle} />
            <div className=" flex justify-center items-center">
        
          <form className="w-1/3"
          onSubmit={handleChange}>
          <div>
            <input
              type="password"
              className={`w-2/3 p-2 text-lg text-white-light bg-transparent border-b border-gray-light hover:shadow-3xl  outline-none text-sm transition duration-150 ease-in-out mb-4`}
              id="old_password"
              placeholder=" Type your old password"
            />
          </div>
          <div>
            <input
              type="password"
              className={`w-2/3 p-2 text-lg text-white-light bg-transparent border-b border-gray-light hover:shadow-3xl  outline-none text-sm transition duration-150 ease-in-out mb-4`}
              id="new_password"
              placeholder=" Type your new password"
            />
          </div>
          <div>
            <input
              type="password"
              className={`w-2/3 p-2 bg-transparent text-lg text-white-light border-b border-gray-light hover:shadow-3xl outline-none text-sm transition duration-150 ease-in-out mb-4`}
              id="new_password_repeat"
              placeholder=" Repeat your new password"
            />
          </div>

          <div className="flex justify-center items-center mt-6">
            <button
              className={` w-1/3 bg-white-light py-2 px-4 text-md text-purple-light font-bold rounded-full hover:shadow-3xl  focus:outline-none `}
            >
              <FormattedMessage id="settings.change"
              >
              Change
              </FormattedMessage>
            </button>
          </div>

         
        </form>
          </div>
         
          <div className="flex justify-center items-center mt-12">
          
          </div>
            
        </div>
    )
}

export default Settings
