import React from "react";
import Login from "./pages/login";
import "./styles.css";
import Community from "./pages/community";
import { Route, Switch, Redirect } from "react-router-dom";
import Post from "./pages/newPost";
import Home from "./pages/home";
import Register from "./pages/register";
import PostComment from "./pages/postComment";
import Settings from './pages/settings'

function App() {
  return (
    <div>
      <Switch>
        <Route exact path="/">
          <Redirect to="/home" component={Home} />
        </Route>
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
        <Route path="/home" component={Home} />
        <Route path="/community/:community_name" component={Community} />
        <Route path="/new_post" component={Post} />
        <Route path="/post/:post_id" component={PostComment} />
        <Route path="/settings" component={Settings}/>
      </Switch>
    </div>
  );
}

export default App;
