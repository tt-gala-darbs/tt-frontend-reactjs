import React, {useState} from "react";
import ReactDOM from "react-dom";
import App from './App';
import './styles.css';
import {HashRouter } from 'react-router-dom';
import Wrapper from "./components/Wrapper";


ReactDOM.render(
    
        <React.StrictMode>
            <HashRouter>
                <Wrapper>
                    <App />
                </Wrapper>
            </HashRouter>
        </React.StrictMode>
   
        ,
     document.getElementById("root"));